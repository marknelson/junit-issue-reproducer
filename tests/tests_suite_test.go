package tests_test

import (
	"github.com/onsi/ginkgo/config"
	"github.com/onsi/ginkgo/reporters"
	"testing"
	"fmt"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestTests(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter(fmt.Sprintf("junit-%d.xml", config.GinkgoConfig.ParallelNode))
	RunSpecsWithDefaultAndCustomReporters(t, "My Test Suite", []Reporter{junitReporter})
}
