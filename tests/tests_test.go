package tests_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Tests", func() {
	It("can pass", func() {
		Expect(1).To(Equal(1))
	})

	It("can be skipped", func() {
		Skip("this test is skipped")
	})
})
